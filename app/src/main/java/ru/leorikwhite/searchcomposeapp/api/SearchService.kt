package ru.leorikwhite.searchcomposeapp.api

import retrofit2.http.GET
import retrofit2.http.Query
import ru.leorikwhite.searchcomposeapp.data.SearchResult

interface SearchService {
    @GET("customsearch/v1/")
    suspend fun getSearchResult(
        @Query("key") key:String,
        @Query("cx") cx:String,
        @Query("q") q:String,
        @Query("start") start:Int
    ): SearchResult

    @GET("customsearch/v1/")
    suspend fun getImageResult(
        @Query("key") key:String,
        @Query("cx") cx:String,
        @Query("q") q:String,
        @Query("searchType") searchType:String,
        @Query("start") start:Int
    ): SearchResult
}