package ru.leorikwhite.searchcomposeapp.data

import ru.leorikwhite.searchcomposeapp.api.SearchService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchResultRepository @Inject constructor(private val searchAPI:SearchService){

    suspend fun getSearchResult(q:String,start:Int):List<Items>{
        return searchAPI.getSearchResult(key,cx,q,start).items
    }

    suspend fun getImageResult(q: String,start: Int):List<Items>{
        return searchAPI.getImageResult(key,cx,q,"image",start).items
    }
    private companion object {
        val key = "AIzaSyB3h1AukquEJ2wjP1_LbheIp6YiCJ92SGU"
        val cx = "b7b943639887041c2"
    }
}