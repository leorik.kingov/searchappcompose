package ru.leorikwhite.searchcomposeapp.data

import com.google.gson.annotations.SerializedName

data class SearchResult(
    @SerializedName("kind") var kind: String? = null,
    @SerializedName("url") var url: Url? = Url(),
    @SerializedName("queries") var queries: Queries? = Queries(),
    @SerializedName("context") var context: Context? = Context(),
    @SerializedName("searchInformation") var searchInformation: SearchInformation? = SearchInformation(),
    @SerializedName("spelling") var spelling: Spelling? = Spelling(),
    @SerializedName("items") var items: ArrayList<Items> = arrayListOf()
)

data class Url(

    @SerializedName("type") var type: String? = null,
    @SerializedName("template") var template: String? = null

)

data class Request(

    @SerializedName("title") var title: String? = null,
    @SerializedName("totalResults") var totalResults: String? = null,
    @SerializedName("searchTerms") var searchTerms: String? = null,
    @SerializedName("count") var count: Int? = null,
    @SerializedName("startIndex") var startIndex: Int? = null,
    @SerializedName("inputEncoding") var inputEncoding: String? = null,
    @SerializedName("outputEncoding") var outputEncoding: String? = null,
    @SerializedName("safe") var safe: String? = null,
    @SerializedName("cx") var cx: String? = null

)

data class NextPage(

    @SerializedName("title") var title: String? = null,
    @SerializedName("totalResults") var totalResults: String? = null,
    @SerializedName("searchTerms") var searchTerms: String? = null,
    @SerializedName("count") var count: Int? = null,
    @SerializedName("startIndex") var startIndex: Int? = null,
    @SerializedName("inputEncoding") var inputEncoding: String? = null,
    @SerializedName("outputEncoding") var outputEncoding: String? = null,
    @SerializedName("safe") var safe: String? = null,
    @SerializedName("cx") var cx: String? = null

)

data class Queries(

    @SerializedName("request") var request: ArrayList<Request> = arrayListOf(),
    @SerializedName("nextPage") var nextPage: ArrayList<NextPage> = arrayListOf()

)

data class Context(

    @SerializedName("title") var title: String? = null

)

data class SearchInformation(

    @SerializedName("searchTime") var searchTime: Double? = null,
    @SerializedName("formattedSearchTime") var formattedSearchTime: String? = null,
    @SerializedName("totalResults") var totalResults: String? = null,
    @SerializedName("formattedTotalResults") var formattedTotalResults: String? = null

)

data class Spelling(

    @SerializedName("correctedQuery") var correctedQuery: String? = null,
    @SerializedName("htmlCorrectedQuery") var htmlCorrectedQuery: String? = null

)

data class CseThumbnail(

    @SerializedName("src") var src: String? = null,
    @SerializedName("width") var width: String? = null,
    @SerializedName("height") var height: String? = null

)


data class Metatags(

    @SerializedName("og:image") var og_image: String? = null,
    @SerializedName("theme-color") var theme_color: String? = null,
    @SerializedName("og:type") var og_type: String? = null,
    @SerializedName("al:ios:app_name") var al_ios_appName: String? = null,
    @SerializedName("og:title") var og_title: String? = null,
    @SerializedName("al:android:package") var al_android_package: String? = null,
    @SerializedName("al:ios:url") var al_ios_url: String? = null,
    @SerializedName("color-scheme") var color_scheme: String? = null,
    @SerializedName("og:description") var og_description: String? = null,
    @SerializedName("al:ios:app_store_id") var al_ios_appStoreId: String? = null,
    @SerializedName("al:android:url") var al_android_url: String? = null,
    @SerializedName("apple-mobile-web-app-status-bar-style") var apple_mobile_web_app_status_bar_style: String? = null,
    @SerializedName("viewport") var viewport: String? = null,
    @SerializedName("mobile-web-app-capable") var mobile_web_app_capable: String? = null,
    @SerializedName("og:url") var og_url: String? = null,
    @SerializedName("al:android:app_name") var al_android_appName: String? = null

)

data class CseImage(

    @SerializedName("src") var src: String? = null

)

data class Pagemap(

    @SerializedName("cse_thumbnail") var cseThumbnail: ArrayList<CseThumbnail> = arrayListOf(),
    @SerializedName("metatags") var metatags: ArrayList<Metatags> = arrayListOf(),
    @SerializedName("cse_image") var cseImage: ArrayList<CseImage> = arrayListOf()

)

data class Items(

    @SerializedName("kind") var kind: String? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("htmlTitle") var htmlTitle: String? = null,
    @SerializedName("link") var link: String? = null,
    @SerializedName("displayLink") var displayLink: String? = null,
    @SerializedName("snippet") var snippet: String? = null,
    @SerializedName("htmlSnippet") var htmlSnippet: String? = null,
    @SerializedName("formattedUrl") var formattedUrl: String? = null,
    @SerializedName("htmlFormattedUrl") var htmlFormattedUrl: String? = null,
    @SerializedName("pagemap") var pagemap: Pagemap? = Pagemap(),
    @SerializedName("image") var image: Image? = null

)

data class Image(
    @SerializedName("contextLink") var contextLink:String? = null,
    @SerializedName("height") var height: Int? = null,
    @SerializedName("width") var width: Int? = null,
    @SerializedName("byteSize") var byteSize: Int? = null,
    @SerializedName("thumbnailLink") var thumbnailLink: String? = null,
    @SerializedName("thumbnailHeight") var thumbnailHeight: Int? = null,
    @SerializedName("thumbnailWidth") var thumbnailWidth: Int? = null,
)
