package ru.leorikwhite.searchcomposeapp.compose

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.leorikwhite.searchcomposeapp.R
import ru.leorikwhite.searchcomposeapp.data.Items
import ru.leorikwhite.searchcomposeapp.ui.theme.BasicWhite
import ru.leorikwhite.searchcomposeapp.ui.theme.BorderGray
import ru.leorikwhite.searchcomposeapp.ui.theme.FocusedBorderGray
import ru.leorikwhite.searchcomposeapp.viewmodels.SearchViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchView(viewModel: SearchViewModel = hiltViewModel()) {
    val query = remember { mutableStateOf(viewModel.query.value) }
    val itemList = remember { mutableStateListOf<Items>() }
    val loading = remember { mutableStateOf(false) }
    val page = remember { mutableStateOf(1) }
    val lazyListState: LazyListState = rememberLazyListState()
    val state = viewModel.state.collectAsState()
    Column {
        OutlinedTextField(
            query.value,
            onValueChange = {
                query.value = it
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    if (query.value != "") {
                        itemList.clear()
                        page.value = 1
                        CoroutineScope(Dispatchers.IO).launch {
                            viewModel.intentChannel.send(
                                SearchViewModel.Intent.GetSearchResult(
                                    query.value,
                                    page.value
                                )
                            )
                        }
                    }
                }
            ),
            placeholder = { Text("Поиск") },
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 1.dp, start = 1.dp, end = 1.dp),
            textStyle = TextStyle(fontSize = 20.sp),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = FocusedBorderGray, // цвет при получении фокуса
                unfocusedBorderColor = BorderGray  // цвет при отсутствии фокуса
            ),
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.search_icon),
                    contentDescription = null
                )
            },


            )

        when (state.value) {
            is SearchViewModel.State.Error -> {
                Text(text = (state.value as SearchViewModel.State.Error).error ?: "error")
            }

            is SearchViewModel.State.Success -> {
                itemList.addAll((state.value as SearchViewModel.State.Success).items)
                loading.value = false
            }

            is SearchViewModel.State.Idle -> {}
            is SearchViewModel.State.Loading -> {
                loading.value = true
            }
        }
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            state = lazyListState
        ) {
            items(itemList) { item ->
                SearchItem(item)
            }
            item {
                if (loading.value) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(
                            modifier = Modifier.size(50.dp),
                            strokeWidth = 2.dp
                        )
                    }
                }
            }

            if (lazyListState.firstVisibleItemIndex != 0 && lazyListState.firstVisibleItemIndex >= itemList.size - 4) {
                if (!loading.value) {
                    page.value += 1
                    CoroutineScope(Dispatchers.IO).launch {
                        viewModel.intentChannel.send(
                            SearchViewModel.Intent.GetSearchResult(
                                query.value,
                                page.value
                            )
                        )
                    }
                }
            }

        }
    }
}

@Composable
private fun SearchItem(items: Items) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 5.dp, start = 5.dp, end = 5.dp),
        shape = RoundedCornerShape(10.dp),
        colors = CardDefaults.cardColors(
            containerColor = BasicWhite
        )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(5.dp),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                AsyncImage(
                    model = items.link,
                    contentDescription = null,
                    modifier = Modifier
                        .size(32.dp)
                        .padding(top = 5.dp, start = 5.dp)
                )
                Text(
                    text = items.displayLink ?: "",
                    Modifier.padding(start = 5.dp)
                )
            }

            Text(
                text = items.title ?: "",
                Modifier.padding(5.dp),
                fontSize = 20.sp
            )
            Text(
                text = items.snippet ?: "",
                Modifier.padding(5.dp)
            )

        }
    }
}
