package ru.leorikwhite.searchcomposeapp.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.staggeredgrid.LazyStaggeredGridState
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.lazy.staggeredgrid.rememberLazyStaggeredGridState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.SubcomposeAsyncImage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.leorikwhite.searchcomposeapp.R
import ru.leorikwhite.searchcomposeapp.data.Items
import ru.leorikwhite.searchcomposeapp.ui.theme.BorderGray
import ru.leorikwhite.searchcomposeapp.ui.theme.FocusedBorderGray
import ru.leorikwhite.searchcomposeapp.viewmodels.SearchViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ImageSearchView(viewModel: SearchViewModel = hiltViewModel()) {
    val query = remember { mutableStateOf("") }
    val itemList = remember { mutableStateListOf<Items>() }
    val loading = remember { mutableStateOf(false) }
    val page = remember { mutableStateOf(1) }
    val lazyGridState: LazyStaggeredGridState = rememberLazyStaggeredGridState()
    Column {
        OutlinedTextField(
            query.value,
            onValueChange = {
                query.value = it
            },
            placeholder = { Text("Поиск") },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    if(query.value!="") {
                        itemList.clear()
                        page.value = 1
                        CoroutineScope(Dispatchers.IO).launch {
                            viewModel.intentChannel.send(SearchViewModel.Intent.GetImageResult(query.value,page.value))
                        }
                    }
                }
            ),
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 1.dp, start = 1.dp, end = 1.dp),
            textStyle = TextStyle(fontSize = 20.sp),
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = FocusedBorderGray, // цвет при получении фокуса
                unfocusedBorderColor = BorderGray  // цвет при отсутствии фокуса
            ),
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.search_icon),
                    contentDescription = null
                )
            },


            )
        val state = viewModel.state.collectAsState()
        when (state.value) {
            is SearchViewModel.State.Error -> {
                Text(text = (state.value as SearchViewModel.State.Error).error ?: "error")
            }

            is SearchViewModel.State.Success -> {
                if(itemList.isEmpty()){
                    itemList.addAll((state.value as SearchViewModel.State.Success).items)
                }
                if(itemList.last()!=(state.value as SearchViewModel.State.Success).items.last()) {
                    itemList.addAll((state.value as SearchViewModel.State.Success).items)
                }
                loading.value = false
            }

            is SearchViewModel.State.Idle -> {}
            is SearchViewModel.State.Loading -> {
                loading.value = true
            }
        }
        LazyVerticalStaggeredGrid(
            columns = StaggeredGridCells.Fixed(2),
            verticalItemSpacing = 4.dp,
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            state = lazyGridState,
            content = {
                items(itemList) { item ->

                    SubcomposeAsyncImage(
                        model = item.link,
                        loading = {
                            CircularProgressIndicator(
                                Modifier
                                    .fillMaxSize()
                                    .height(20.dp)
                                    .width(20.dp)
                            )
                        },
                        contentScale = ContentScale.Crop,
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(
                                if ((item.image?.height ?: 1) > (item.image?.width
                                        ?: 1)
                                ) 400.dp else {
                                    200.dp
                                }
                            )
                            .background(Color.LightGray)
                    )
                }
                if(lazyGridState.firstVisibleItemIndex!=0&&lazyGridState.firstVisibleItemIndex>=itemList.size-9){
                    if(!loading.value) {
                        page.value += 1
                        CoroutineScope(Dispatchers.IO).launch {
                            viewModel.intentChannel.send(
                                SearchViewModel.Intent.GetImageResult(
                                    query.value,
                                    page.value
                                )
                            )
                        }
                    }
                }
            },
            modifier = Modifier.fillMaxSize()
        )
        if (loading.value) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(
                    modifier = Modifier.size(50.dp),
                    strokeWidth = 2.dp
                )
            }
        }
    }
}