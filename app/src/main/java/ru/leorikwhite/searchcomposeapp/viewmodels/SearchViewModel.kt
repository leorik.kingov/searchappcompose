package ru.leorikwhite.searchcomposeapp.viewmodels

import android.content.Intent
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import ru.leorikwhite.searchcomposeapp.data.Items
import ru.leorikwhite.searchcomposeapp.data.SearchResult
import ru.leorikwhite.searchcomposeapp.data.SearchResultRepository
import javax.inject.Inject
import javax.inject.Singleton

@HiltViewModel
class SearchViewModel @Inject constructor(private val searchResultRepository: SearchResultRepository) :
    ViewModel() {

    val intentChannel = Channel<Intent>(Channel.BUFFERED)
    private val _state = MutableStateFlow<State>(State.Idle)
    val state: StateFlow<State>
        get() = _state

    private var _query = mutableStateOf("")
    val query: MutableState<String>
        get() = _query
    init {
        handleIntents()
    }

    sealed class Intent {
        data class GetSearchResult (var query: String?=null, var page:Int):Intent()
        data class GetImageResult (var query: String?=null,var page:Int):Intent()
    }

    sealed class State {
        object Idle : State()
        object Loading : State()
        data class Success(val items: List<Items>) : State()
        data class Error(val error: String?) : State()
    }


    private fun handleIntents() {
        viewModelScope.launch {
            intentChannel.consumeAsFlow().collect { intent ->
                when (intent) {
                    is Intent.GetSearchResult -> {
                        _query.value = intent.query ?: ""
                        getSearchResult(intent.page, intent.query ?: "")
                    }
                    is Intent.GetImageResult -> {
                        _query.value = intent.query ?: ""
                        getImageResult(intent.page,intent.query ?: "")
                    }
                }
            }
        }
    }

    private fun getSearchResult(page: Int, query: String) {
        viewModelScope.launch {
            _state.value = State.Loading
            _state.value = try {
                State.Success(searchResultRepository.getSearchResult(query,page))
            } catch (e:Exception){
                State.Error(e.localizedMessage)
            }
        }
    }
    private fun getImageResult(page: Int, query: String) {
        viewModelScope.launch {
            _state.value = State.Loading
            _state.value = try {
                State.Success(searchResultRepository.getImageResult(query,page))
            } catch (e:Exception){
                State.Error(e.localizedMessage)
            }
        }
    }
}